@extends('layout.master')
@section('judul')
    Detail Cast ID {{ $cast->id }}
@endsection

@section('content')
<h2 class="text-primary">{{$cast->nama}}</h2>
<p>Umur {{$cast->umur}} tahun</p>
<p>{{$cast->bio}}</p>
@endsection