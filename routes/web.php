<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CastController;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function(){
    return view('pages.home');
});


Route::get('/table', function(){
    return view('pages.table');
});

Route::get('/data-tables', function() {
    return view('pages.data-tables');
});

Route::get('/cast', 'CastController@index');

Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast/{cast_id}', 'CastController@show');

Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');





